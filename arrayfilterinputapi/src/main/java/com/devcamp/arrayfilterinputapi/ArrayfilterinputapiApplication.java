package com.devcamp.arrayfilterinputapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArrayfilterinputapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArrayfilterinputapiApplication.class, args);
	}

}
