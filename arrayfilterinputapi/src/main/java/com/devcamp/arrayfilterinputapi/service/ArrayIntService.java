package com.devcamp.arrayfilterinputapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class ArrayIntService {
    private int[] rainbowsList = { 1, 23, 32, 43, 54, 65, 86, 10, 15, 16, 18 };

    public ArrayList<Integer> searchRainbows(int pos) {
        ArrayList<Integer> rainbows = new ArrayList<>();

        for (int rainbow : rainbowsList) {
            if (rainbow > pos) {
                rainbows.add(rainbow);
            }
        }

        return rainbows;
    }

    
    public int getRainbow(int index) {
        int rainbow = 0;

        if (index >= 0 && index <= 6)
            rainbow = rainbowsList[index];

        return rainbow;
    }
}
