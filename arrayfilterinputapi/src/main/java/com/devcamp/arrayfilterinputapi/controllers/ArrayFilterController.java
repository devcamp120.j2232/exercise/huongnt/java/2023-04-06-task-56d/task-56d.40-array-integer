package com.devcamp.arrayfilterinputapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.arrayfilterinputapi.service.ArrayIntService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ArrayFilterController {
    @Autowired
    ArrayIntService arrayIntService;
    @GetMapping("/array-int-request-query")
    public ArrayList<Integer> searchRainbows(@RequestParam(value="pos") int pos) {

        return arrayIntService.searchRainbows(pos);
    }
    
    
    @GetMapping("/array-int-param/{index}")
    public int getRainbow(@PathVariable int index) {

        return arrayIntService.getRainbow(index);
    }
}
